var Promise = require('bluebird');



var Controller = function(){};


Controller.prototype.getConnection = function(pool){
    var self = this;
    return new Promise(function(resolve, reject) {
        try {
            pool.getConnection(function (err, connection) {
                if (err) {
                    reject({"http_code": 500, "code": 841, "message": "Error in connection database"});
                }
                else {
                    resolve(connection);
                }
            });
        }catch(e){
            reject({"code": 841, "message": "Error in connection database"});
        }
    });
};

Controller.prototype.getTransactionTypes=function(pool){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select * from transaction_types", function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code": 500, "code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.insertTermsConditions=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("insert into term_conditions set ? ", {
                    store: data.store,
                    transaction_type_id: data.transaction_type_id,
                    text: data.text
                }, function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.createSession=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("insert into sessions set ? ", {
                    store: data.store,
                    user: data.user,
                    uuid: data.uuid,
                    model: data.model,
                    token: data.token
                }, function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        //console.log(err);
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.isSession=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select count(*) as c from sessions where token=?", [data.token], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.getSession=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select * from sessions where token=?", [data.token], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};


Controller.prototype.getUserSessions=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select * from sessions where store=? and user=?", [data.store, data.user], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.deleteSession=function(pool,data){
    var self = this;
   // console.log(data);
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("delete from sessions where token=? and store=? and user=?", [data.token, data.store, data.user], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};


Controller.prototype.updateTermsConditions=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("update term_conditions set ? where store=? and transaction_type_id=?", [{text: data.text}, data.store, data.transaction_type_id], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.deleteTermsConditions=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("delete from term_conditions where store=? and transaction_type_id=?", [data.store, data.transaction_type_id], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.getTermsConditions=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select * from term_conditions where store=? and transaction_type_id=?", [data.store, data.transaction_type_id], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};


Controller.prototype.insertSignature=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){

        var config = require('./config'); // get our config file
        var base64Data = data.signature.replace(/^data:image\/png;base64,/, "");
        var uuid = require('node-uuid');
        var fileId=uuid.v1()+".png";
        var fs = require('fs');

        fs.writeFile(config.signatures_path+fileId, base64Data, 'base64', function(err){
            if(!err){
                self.getConnection(pool)
                    .then(function(connection){
                        connection.query("insert into signatures set ? ", {
                            store: data.store,
                            user: data.user,
                            transaction_type: data.transaction_type,
                            transaction_id: data.transaction_id,
                            signature: fileId
                        }, function (err, rows) {
                            connection.release();
                            if (!err) {
                                resolve({
                                    code: 0
                                });
                            } else {
                                console.log(err);
                                reject({"http_code":500,"code": 843, "message": "Operation failed"});
                            }
                        });
                    },function(err2){
                        console.log(err2);
                        reject(err2);
                    });

            }else{

                reject({"http_code":500, "code": 852, "message": "Operation failed"});
            }
        });



    });
};

Controller.prototype.getSignatureFileName=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select signature from signatures where store=? and user=? and transaction_type=? and transaction_id=?", [data.store, data.user, data.transaction_type, data.transaction_id], function (err, rows) {
                    connection.release();
                    if (!err) {
                        resolve({
                            code: 0,
                            data: rows
                        });
                    } else {
                        reject({"http_code":500,"code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};

Controller.prototype.updateSignature=function(pool,data){
    var self = this;
    return new Promise(function(resolve, reject){
        self.getConnection(pool)
            .then(function(connection){
                connection.query("select signature from signatures where store=? and user=? and transaction_type=? and transaction_id=?", [data.store, data.user, data.transaction_type, data.transaction_id], function (err, rows) {
                    console.log(rows);
                    if (!err) {
                        if (rows.length != 0) {
                            var config = require('./config'); // get our config file
                            var oldFileName = rows[0].signature;
                            var oldFilePath = config.signatures_path+oldFileName;

                            var fs = require('fs');
                            fs.unlink(oldFilePath,function(err){
                                /*if(!err){

                                }else{
                                    reject({"http_code":500, "code": 853, "message": "Operation failed"});
                                }*/
                            });

                            var base64Data = data.signature.replace(/^data:image\/png;base64,/, "");
                            var uuid = require('node-uuid');
                            var fileId=uuid.v1()+".png";
                            fs.writeFile(config.signatures_path+fileId, base64Data, 'base64', function(err) {
                                if(!err){
                                    connection.query("update signatures set ? where store=? and user=? and transaction_type=? and transaction_id=? ", [{signature: fileId}, data.store, data.user, data.transaction_type, data.transaction_id], function (err, rows) {
                                        connection.release();
                                        if (!err) {
                                            resolve({
                                                code: 0
                                            });
                                        } else {
                                            console.log(err);
                                            reject({"http_code":500, "code": 843, "message": "Operation failed"});
                                        }
                                    });
                                }else{
                                    reject({"http_code":500, "code": 852, "message": "Operation failed"});
                                }
                            });

                        } else {
                            resolve({
                                code: 0
                            });
                        }

                    } else {
                        console.log(err);
                        reject({"http_code":500, "code": 843, "message": "Operation failed"});
                    }
                });
            },function(err){
                reject(err);
            });
    });
};





module.exports = new Controller();
