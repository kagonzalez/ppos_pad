var express = require('express');
var app = express();
var logger = require('morgan');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var controller = require('./controller');
var cors = require('cors');

var fs = require('fs');



var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file

var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: config.bd.host,
    user: config.bd.user,
    password: config.bd.password,
    database: config.bd.database,
    debug: false
});


// =======================
// configuration =========
// =======================
var port = process.env.PORT || 8081; // used to create, sign, and verify tokens
app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

// use morgan to log requests to the console
app.use(logger('dev'));


// =======================
// start the secure server ======
// =======================
/*var https = require('https');
https.createServer({
    key: fs.readFileSync(config.certificate_key_file),
    cert: fs.readFileSync(config.certificate_file)
}, app).listen(port);
*/

var http = require('http');
http.createServer(app).listen(port);



// get an instance of the router for api routes
var apiRoutes = express.Router();


// ---------------------------------------------------------
// authentication (no middleware necessary since this isnt authenticated)
// ---------------------------------------------------------

apiRoutes.post('/authenticate', function (req, res) {
    // find the user
    "use strict";

    if (req.body.username && req.body.password && req.body.store && req.body.uuid && req.body.model) {
        var soap = require('soap');
        var url = 'https://' + req.body.store + '.procellpos.com/app/MobileApi?wsdl';
        var soapHeader = ''//xml string for header

        soap.createClient(url, function (err, client) {
            try {
                client.addSoapHeader(soapHeader);
                var args = {
                    name: req.body.username,
                    password: req.body.password
                };
                client.pPosStoreDetailsApi(args, function (err, result) {
                    if (err) {
                        res.status(500).json({code: 812, message: 'Authentication failed'});
                    }
                    var out = '';
                    if (result.return.msg && result.return.msg == 'E_LOGIN') {
                        out = {code: 821, message: 'Authentication failed'};
                        res.status(401).json(out)
                    } else {
                        // if user is found and password is right
                        // create a token
                        var payload = {
                            storeId: result.return.storeId,
                            user: req.body.username,
                            uuid:req.body.uuid,
                            model:req.body.model
                        };

                        var token = jwt.sign(payload, app.get('superSecret'), {
                            expiresIn: '360d' // expires in 24 hours
                        });

                        var args = {
                            store: result.return.storeId,
                            user: req.body.username,
                            uuid:req.body.uuid,
                            model:req.body.model,
                            token: token
                        };

                        controller.createSession(pool,args).then(function (data) {
                            out = {
                                code: 0,
                               // storeId: result.return.storeId,
                               // storeName: result.return.storeName,
                               // storeAddr: result.return.storeAddr,
                               // username: req.body.username,
                                role: (req.body.username == 'admin') ? 'admin' : 'user',
                                token: token
                            };
                            res.status(200).json(out)
                        }, function (err) {
                            res.status(err.http_code).json(err);
                        });

                    }
                });
            } catch (e) {
                res.status(500).json({code: 811, message: 'Authentication failed'});
            }
        });
    } else {
        res.status(400).json({code: 831, message: 'Authentication failed'});
    }

});
apiRoutes.get('/signatures/:file', function (req, res) {
    var fs = require('fs');
    var filename = config.signatures_path + req.params.file;
    fs.exists(filename, function (exists) {
        if (!exists) {
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res.end('404 Not Found');
            return;
        }

        res.writeHead(200, 'image/png');
        var fileStream = fs.createReadStream(filename);
        fileStream.pipe(res);
    });

});


apiRoutes.post('/validate-session', function (req, res) {
    if (req.body.token){
        var token = req.body.token;
        jwt.verify(token, app.get('superSecret'), function (err, decoded) {
            if (err) {
                return res.status(200).json({code: 823, message: 'Failed to authenticate token.'});
            } else {
                controller.isSession(pool,{token:token}).then(function (data) {
                    if(typeof data.code !== 'undefined'){
                        if(data.data[0].c>0){
                            res.status(200).send({
                                code: 0
                            });
                        }else{
                            res.status(200).send({
                                code: 823,
                                message: 'Token not actived'
                            });
                        }
                    }else{
                        res.status(200).send({
                            code: 823,
                            message: 'Token not actived'
                        });
                    }

                }, function (err) {
                    res.status(200).send({
                        code: 823,
                        message: 'Token not actived'
                    });
                });
            }
        });

    }else {
        res.status(400).json({code: 831, message: 'Operation failed'});
    }

});


// ---------------------------------------------------------
// route middleware to authenticate and check token
// ---------------------------------------------------------
apiRoutes.use(function (req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query['token'] || req.headers['x-access-token'];

    // decode token
    if (token) {
        jwt.verify(token, app.get('superSecret'), function (err, decoded) {
            if (err) {
                return res.status(401).json({code: 823, message: 'Failed to authenticate token.'});
            } else {
                controller.isSession(pool,{token:token}).then(function (data) {
                    if(typeof data.code !== 'undefined'){
                       if(data.data[0].c>0){
                           req.payload = decoded;
                           next();
                       }else{
                            res.status(401).send({
                               code: 823,
                               message: 'Token not actived'
                           });
                       }
                    }else{
                         res.status(401).send({
                            code: 823,
                            message: 'Token not actived'
                        });
                    }

                }, function (err) {
                     res.status(401).send({
                        code: 823,
                        message: 'Token not actived'
                    });
                });
            }
        });

    } else {
         res.status(401).send({
            code: 824,
            message: 'No token provided.'
        });

    }

});


apiRoutes.get('/transactions-types', function (req, res) {
    controller.getTransactionTypes(pool).then(function (data) {
        res.status(200).json(data);
    }, function (err) {
        res.status(err.http_code).json(err);
    });
});

/*OK- Create terms and conditions*/
apiRoutes.post('/terms-conditions/', function (req, res) {

    var payload = req.payload;

    if (req.body.text && req.body.transaction_type_id) {
        controller.insertTermsConditions(pool, {
            store: payload.storeId,
            text: req.body.text,
            transaction_type_id: req.body.transaction_type_id
        }).then(function (data) {
            res.status(200).json(data);
            res.end();
            return;
        }, function (err) {
            res.status(err.http_code).json(err);
        });
    } else {
        res.status(400).json({code: 831, message: 'Operation failed'});
    }
});

/*OK - Get a terms and conditions*/
apiRoutes.get('/terms-conditions/:transaction_type', function (req, res) {

    var payload = req.payload;
    controller.getTermsConditions(pool, {
        store: payload.storeId,
        transaction_type_id: req.params.transaction_type
    }).then(function (data) {
        res.status(200).json(data);
    }, function (err) {
        res.status(err.http_code).json(err);
    });

});

/*OK - Update a terms and conditions*/
apiRoutes.put('/terms-conditions/:transaction_type', function (req, res) {

    var payload = req.payload;
    if (req.body.text) {
        controller.updateTermsConditions(pool, {
            store: payload.storeId,
            text: req.body.text,
            transaction_type_id: req.params.transaction_type
        }).then(function (data) {
            res.status(200).json(data);
        }, function (err) {
            res.status(err.http_code).json(err);
        });
    } else {
        res.status(400).json({code: 831, message: 'Operation failed'});
    }
});

/*OK - Delete a terms and conditions*/
apiRoutes.delete('/terms-conditions/:transaction_type', function (req, res) {
    var payload = req.payload;
    if (req.body.text) {
        controller.deleteTermsConditions(pool, {
            store: payload.storeId,
            transaction_type_id: req.params.transaction_type
        }).then(function (data) {
            res.status(200).json(data);
        }, function (err) {
            res.status(err.http_code).json(err);
        });
    } else {
        res.status(400).json({code: 831, message: 'Operation failed'});
    }
});


apiRoutes.put('/sessions/', function (req, res) {
    var payload = req.payload;
    if (req.body.delete_token) {

        controller.deleteSession(pool,{
            token:req.body.delete_token,
            store:payload.storeId,
            user:payload.user
        }).then(function (data) {
            res.status(200).json(data);
        }, function (err) {
            res.status(err.http_code).json(err);
        });
    } else {
        res.status(400).json({code: 831, message: 'Operation failed'});
    }
});

apiRoutes.get('/sessions/', function (req, res) {

    var payload = req.payload;
        controller.getUserSessions(pool,{
            store:payload.storeId,
            user:payload.user
        }).then(function (data) {
            res.status(200).json(data);
        }, function (err) {
            res.status(err.http_code).json(err);
        });

});


/*OK - Create terms and conditions*/
apiRoutes.post('/signatures/', function (req, res) {
     var payload = req.payload;
    if (req.body.transaction_type && req.body.transaction_id && req.body.signature) {
        controller.insertSignature(pool, {
            store: payload.storeId,
            user: payload.user,
            transaction_type: req.body.transaction_type,
            transaction_id: req.body.transaction_id,
            signature:req.body.signature
        }).then(function (data) {
            res.status(200).json(data);
        }, function (err) {
            res.status(err.http_code).json(err);
        });


    } else {
        res.status(400).json({code: 831, message: 'Operation failed'});
    }
});

apiRoutes.get('/signatures/:transaction_type/:transaction_id', function (req, res) {
    var payload = req.payload;
    controller.getSignatureFileName(pool, {
        store: payload.storeId,
        user: payload.user,
        transaction_type: req.params.transaction_type,
        transaction_id: req.params.transaction_id
    }).then(function (data) {
        res.status(200).json(data);
    }, function (err) {
        res.status(err.http_code).json(err);
    });

});

apiRoutes.put('/signatures/:transaction_type/:transaction_id', function (req, res) {
    if (req.body.signature) {
        var payload = req.payload;

        controller.updateSignature(pool, {
            store: payload.storeId,
            user: payload.user,
            transaction_id: req.params.transaction_id,
            transaction_type: req.params.transaction_type,
            signature: req.body.signature
        }).then(function (data) {
            res.status(200).json(data);
        }, function (err) {
            res.status(err.http_code).json(err);
        });
    } else {
        res.status(400).json({code: 831, message: 'Operation failed'});
    }
});


// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes);
